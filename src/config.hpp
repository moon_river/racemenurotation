#pragma once

#define TOML_ENABLE_FORMATTERS 0
#define TOML_ENABLE_WINDOWS_COMPAT 0
#include <toml++/toml.h>

struct config
{
    static inline uint32_t key_code { 257 };
    static inline float min_rotate_speed = 3.f;
    static inline float max_rotate_speed = 12.f;

    static void load()
    {
        constexpr std::string_view config_path { "Data\\SKSE\\Plugins\\PlayerRotation.toml"sv };

        if (!std::filesystem::exists(config_path))
        {
            logger::warn("Could not find config at {}"sv, config_path);
            return;
        }

        try
        {
            toml::table tbl = toml::parse_file(config_path);  
            key_code = tbl["KeyCode"].value_or(key_code);
            min_rotate_speed = tbl["MinimumRotationSpeed"].value_or(min_rotate_speed);
            max_rotate_speed = tbl["MaximumRotationSpeed"].value_or(max_rotate_speed);
        } 
        catch (const toml::parse_error& err)
        {
            logger::error("{}"sv, err.description());
            return;
        }
    }
};